'use strict';
/* ================================================================================================================================ */
var wantToTestDistance=false;         // in exportData and getGeoJson !
/* ================================================================================================================================ */



// GLOBAL VARBIABLES
var markers=[],
	coord=[],
	route=[],
	choiceOfVeh=[],
	lines=[],
	vehicle='foot',
	dangerPoints=[],
	dangerMessage=[],
	storedDevice=[],
	dangerMarkers=[],
	
	// test variables
	isStartPressed=0,
	isEndPressed=0,
	isClockPressed=0,
	isFirstClicked=0,
	isEndClicked=0,
	
	// inputs (need to be global)
	startTime='',
	startDate='',
	amount=0,
	id=null,
	
	// length of separated route segments (in meters)
	DIST = 1;

/* ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
// MAP
var myID = 'fritzeflink.ipkbgifo';
var accessToken ='pk.eyJ1IjoiZnJpdHplZmxpbmsiLCJhIjoiR1ZHMTQ5VSJ9.pvciELn0vNNaNiKaA6Cr0g';
//var myID = 'brucknerdominik.m5c579e8';
//var accessToken = 'pk.eyJ1IjoiYnJ1Y2tuZXJkb21pbmlrIiwiYSI6InNwY2IwbDQifQ.JavChGjj-uuFJvXuMr28Zg';
var apiKey = '0572789c-f15c-4700-b999-e318402d6530';

// leaflet                 <-- no pedestrian-routing , inf routings per day
// graphhopper             <--    pedestrian&bike-routing available , only 500 routings per day 
// mapzen                  <--    --------------"------------------ , limited by unknown amount : https://github.com/mapzen/routing/wiki/OSRM-Service-Details-and-Terms-of-Use , works most time 

var routers = 'mapzen';

var map = L.map('map', myID, {
    zoomControl: true,
	scrollWheelZoom: true,
	attributionControl: false
}).setView([48.15, 11.59], 13);
L.tileLayer('http://{s}.tiles.mapbox.com/v3/{mapId}/{z}/{x}/{y}.png?access_tolen={accessToken}', {
    maxZoom: 18,
	mapId: myID,
	accessToken: accessToken
}).addTo(map);
/* ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

initialize();


// FUNCTIONS
// INITIALIZATION
/**
If markers are available at the local storage of browser then load them with additional design and properties & calculate route afterwards
**/

function initialize() {
	dangerPoints=[];
	dangerMessage=[];
	dangerMarkers=[];
	lines=[];
	route=[];
	choiceOfVeh=[];
	markers=[];
	storedDevice=[];
	coord=[];
	vehicle = 'foot';
	hideInputs();
	document.getElementById('rectangle').style.top =  0-4 + '%';
	if(getStoredValue('time')!=null) {
		startTime = getStoredValue('time')[0];
		document.getElementById('time').value = startTime;
	}
	if(getStoredValue('date')!=null) {
		startDate = getStoredValue('date')[0];
		document.getElementById('date').value = startDate;
	}
	if(getStoredValue('amount')!=null) {
		amount = getStoredValue('amount')[0];
	}
	if(startTime!=null && startTime!=null) {
		isClockPressed = 1;
	}
	if(getStoredValue('markers')!=null) {
		var storedMarkers = getStoredValue('markers');
		var test = storedMarkers[0].split("(");
		var i;
		if (test[0]=='LatLng') {
			if(getStoredValue('device')!=null) {
				storedDevice = getStoredValue('device')[0].split(',');
				var value;
				switch (storedDevice[storedDevice.length-1]) {
					case 'foot':
						value = 0;
						break;
					case 'bike':
						value = 1;
						break;
					case 'bicycle':
						value = 1;
						break;
					case 'car':
						value = 2;
						break;
					case 'bus':
						value = 3;
						break;
					case 'underground':
						value = 4;
						break;
					default: console.log('Wrong or no device identifier at local storage!');
				}
				document.getElementById('rectangle').style.top = value * 20 - 4 + '%';
			}
			getStoredMarkers(storedMarkers);
			var ml = markers.length;
			switch(ml) {
				case 1:
					isStartPressed = 1;
					document.getElementById('start').value = getStoredValue('address1');
					break;
				case 2:
					isStartPressed = 1;
					isEndPressed = 1;
					document.getElementById('start').value = getStoredValue('address1');
					document.getElementById('end').value = getStoredValue('address2');
					document.getElementById('transportation').style.zIndex="3";
					break;
				default:
					isStartPressed = 1;
					isEndPressed = 1;
					isFirstClicked = 1;
					markers[0].closePopup();
					document.getElementById('start').value = getStoredValue('address1');
					document.getElementById('end').value = getStoredValue('address2');
					document.getElementById('transportation').style.zIndex="3";
			}
		}
	}
	if(isClockPressed==1 && isStartPressed==1 && isEndPressed==1 && amount!=0) {
		showAllInputs();
		centerToStartAndEndpoint();
	} else {
		document.getElementById('firstInput').style.zIndex="2";
		showInput('fstart');
		showInput('fstartOK');
	}
	if(getStoredValue('isEndClicked')!=null) {
		isEndClicked = getStoredValue('isEndClicked')[0];
		hideInputs();
		startDangerReport();
	}
}

function hideInputs() {
	document.getElementById('date').style.display='none';
	document.getElementById('time').style.display='none';
	document.getElementById('start').style.display='none';
	document.getElementById('end').style.display='none';
	document.getElementById('clockOK').style.display='none';
	document.getElementById('startOK').style.display='none';
	document.getElementById('endOK').style.display='none';
	document.getElementById('startRouting').style.display='none';
	document.getElementById('endOfRouting').style.display='none';
	document.getElementById('removeAllMarkersButton').style.display='none';
	
	document.getElementById('fstart').style.display='none';
	document.getElementById('fstartOK').style.display='none';
	document.getElementById('fend').style.display='none';
	document.getElementById('fendOK').style.display='none';
	document.getElementById('fdate').style.display='none';
	document.getElementById('ftime').style.display='none';
	document.getElementById('fclockOK').style.display='none';
	document.getElementById('famount').style.display='none';
	document.getElementById('famountOK').style.display='none';
	document.getElementById('fstartRouting').style.display='none';
	document.getElementById('firstInput').style.zIndex="-1";
	document.getElementById('transportation').style.zIndex="-1";
	document.getElementById('endOfRoutingAndDanger').style.zIndex = '-1';
	document.getElementById('backToRouting').style.zIndex = '-1';
	document.getElementById('dangerReport').style.zIndex='-1';
}

function showInput(key) {
	document.getElementById(key).style.display='block';
}

function showAllInputs() {
	document.getElementById('date').style.display='block';
	document.getElementById('time').style.display='block';
	document.getElementById('start').style.display='block';
	document.getElementById('end').style.display='block';
	document.getElementById('clockOK').style.display='block';
	document.getElementById('startOK').style.display='block';
	document.getElementById('endOK').style.display='block';
	document.getElementById('startRouting').style.display='block';
	document.getElementById('endOfRouting').style.display='block';
	document.getElementById('removeAllMarkersButton').style.display='block';
	document.getElementById('date').value=startDate;
	document.getElementById('time').value=startTime;
	document.getElementById('start').value = getStoredValue('address1');
	document.getElementById('end').value = getStoredValue('address2');
}


// HANDLE MOUSE CLICKS ON MAP 
/**
handle clicks on map (together with the "DRAW AND DELETE MARKERS"-section some sort of main program)
**/

map.on('click',function(e) {
	if(isEndClicked===0) {
		if(isStartPressed===1 && isEndPressed===1 && isClockPressed===1 && isFirstClicked===1) {
			addMarker(e);
			if(markers.length>3) {
				addRoute(markers.length-4);
			}
			changeIcon(markers.length-2);
		} else {
			markers[0].openPopup();
		}
	} else {
		console.log('map clicked during danger report');
	}
});


// DRAW AND DELETE MARKERS
/** 
addMarker: add Marker to markers-array and add to map
onPopupOpen: delete marker by clicking button in popup
deleteMarker: remove marker in markers-array
updatePosition: update of position of dragged marker -> new position in markers-array and route is recalculated  
**/

function addMarker(e) {
	var newMarker = L.marker([e.latlng.lat , e.latlng.lng],{'draggable': true});
	var zwVeh = vehicle;
	if(vehicle=='bicycle'){zwVeh='bike';}
	if(vehicle=='bus'){zwVeh='H';}
	newMarker.options.icon  = L.icon({iconUrl:'../images/'+zwVeh+'.png' , iconSize :[40,40]});
	if(markers.length==3) {
		map.removeLayer(markers[markers.length-1]);
		markers[markers.length-1].options.icon = L.icon({iconUrl:'../images/'+zwVeh+'.png' , iconSize :[40,40]});
		markers[markers.length-1].addTo(map);
	}
	var zw = markers.length-1;
	newMarker.bindPopup('<input type="button" value="Wegpunkt bis hierher löschen" class="marker-delete-button"/>');
	newMarker.on('popupopen', onPopupOpen);
	newMarker.on('dragend',updatePosition);
	markers.push(newMarker);
	markers[markers.length-1].addTo(map);
	return markers;
}

function changeIcon(index) {
	var zwVeh;
	if(vehicle=='bicycle'){zwVeh='bike';}else{zwVeh=vehicle;}
	if(index>2 && choiceOfVeh[index-3] != vehicle) {
		map.removeLayer(markers[index]);
		var zwVeh2 = choiceOfVeh[index-3];
		if(zwVeh2=='bicycle'){zwVeh2='bike';}
		markers[index].options.icon = L.icon({iconUrl:'../images/'+zwVeh2+'To'+zwVeh+'.png' , iconSize :[100,80]});
		markers[index].addTo(map);
	}
}

function onPopupOpen() {
    var tempMarker = this;
    $('.marker-delete-button:visible').click(function () {
		var index = markers.indexOf(tempMarker);
		if(isEndClicked==0) {
			if(index>3) {
				var zwVeh = choiceOfVeh[index-4];
				if(zwVeh=='bicycle'){zwVeh='bike';}
				if(zwVeh=='bus'){zwVeh='H';}
				map.removeLayer(markers[index-1]);
				markers[index-1].options.icon = L.icon({iconUrl:'../images/'+zwVeh+'.png' , iconSize :[40,40]});
				markers[index-1].addTo(map);
			}
			deleteMarker(index);
			deleteRoute(index);
		} else {
			map.removeLayer(tempMarker);
			var index2 = dangerPoints.indexOf(tempMarker);
			dangerPoints.splice(index2,1);
			dangerMessage.splice(index2,1);
		}
    });
}

function deleteMarker(index) {
	var i;
	for(i=index;i<markers.length;i++) {
		map.removeLayer(markers[i]);
	}
	markers = markers.splice(0,index);
	if(index==2){
		markers[0].openPopup();
		isFirstClicked=0;
	}
}

function updatePosition() {
	var tempMarker = this;
	var index = markers.indexOf(tempMarker);
	var m = tempMarker.getLatLng();
	markers[index]._latlng.lat=m.lat;
	markers[index]._latlng.lng=m.lng;
	if(markers.length>3) {
		updateRoute(index);
	}
}


// ROUTING
/**
addRoute: calculate a route between two markers
updateRoute: delete old route and calculates new one on dragged marker basis
deleteRoute: the route from the clicked marker to the end is erased
getCompleteRoute: get the complete Route on markers-array basis (only used at initialization)
getTotalDistance: get distance of route
getTime: get needed time for route (in dependency of speed)
**/

function addRoute(mk) {	
	var myRouter;
	if(vehicle == 'foot' || vehicle == 'bike' || vehicle == 'bicycle') {
		switch (routers) {
			case 'graphhopper':       //graphhopper
				myRouter = L.Routing.graphHopper(apiKey,{
					urlParameters:{
						vehicle: vehicle                           
					}
				});
				break;
			case 'mapzen':       // mapzen
				myRouter = L.Routing.osrm({
					serviceUrl: 'http://osrm.mapzen.com/' + vehicle + '/viaroute'
				});
				break;
			default:
				console.log('wrong router index');
		}
		route[mk] = L.Routing.control({
			waypoints:[
				markers[mk+2]._latlng,
				markers[mk+3]._latlng
			], 
			routeLine: function(route) {
				var line = L.Routing.line(route, {
					addWaypoints: false,
					routeWhileDragging: false,
					autoRoute: true,
					useZoomParameter: false,
					draggableWaypoints: false,
				});
				line.eachLayer(function(l) {
					l.on('click', function(e) {
						if(isEndClicked==1){addDangerMarker(e);}
					});
				});
				return line;
			},
			router: myRouter,
			show:false
		});
		route[mk].addTo(map);
		route[mk].on('routeselected', function(e) {
			coord[mk] = e.route;
		});
	}
	
	if(vehicle == 'car' || vehicle == 'bus' || vehicle == 'underground') {
		var polyline = L.polyline(
			[markers[mk+2]._latlng , markers[mk+3]._latlng],
			{
				color: 'green',
				weight: 5,
				opacity: 0.7,
				dashArray: '20,15',
				lineJoin: 'round'
			}
		);
		if(vehicle == 'bus') {polyline.options.color='yellow';}
		if(vehicle == 'underground') {polyline.options.color='blue';}
		lines[mk] = polyline;
		lines[mk].addTo(map);
		route[mk]='undefined';
	}
	
	choiceOfVeh[mk] = vehicle;
}

function updateRoute(index) {
	if(index>2 && index<markers.length-1) {
		if(route[index-3]!='undefined'){route[index-3].removeFrom(map);}
		if(route[index-2]!='undefined'){route[index-2].removeFrom(map);}
		if(typeof lines[index-3]!='undefined') {map.removeLayer(lines[index-3]);}
		if(typeof lines[index-2]!='undefined') {map.removeLayer(lines[index-2]);}
		vehicle = choiceOfVeh[index-3];
		addRoute(index-3);
		vehicle = choiceOfVeh[index-2];
		addRoute(index-2);
	}
	if(index==2) {
		if(route[index-2]!='undefined'){route[index-2].removeFrom(map);}
		if(typeof lines[index-2]!='undefined') {map.removeLayer(lines[index-2]);}
		vehicle = choiceOfVeh[index-2];
		addRoute(index-2);
	}
	if(index==markers.length-1) {
		if(route[index-3]!='undefined'){route[index-3].removeFrom(map);}
		if(typeof lines[index-3]!='undefined') {map.removeLayer(lines[index-3]);}
		vehicle = choiceOfVeh[index-3];
		addRoute(index-3);
	}
}

function deleteRoute(index) {
	var i;
	if(index>3) {
		for (i=index-3;i<route.length;i++) {
			if(route[i]!='undefined'){route[i].removeFrom(map);}
			if(typeof lines[i]!='undefined'){map.removeLayer(lines[i]);}
		}
		route = route.slice(0,index-3);
		coord = coord.slice(0,index-3);
		lines = lines.slice(0,index-3);
		choiceOfVeh = choiceOfVeh.slice(0,index-3);
	} else {
		for(i=0;i<route.length;i++) {
			if(route[i]!='undefined'){route[i].removeFrom(map);}
			if(typeof lines[i]!='undefined'){map.removeLayer(lines[i]);}
		}
		route = [];
		coord = [];
		lines = [];
		choiceOfVeh = [];
	}
}


// HANDLE BUTTONS AT MAP
/**
startAndEndPointDraw: add start and endpoitn to markers-array and add it to map
exportData: exports data and direct user to ThankYou.html while clicking the "Fertig"-button
centerToStartAndEndpoint: after clicking the "OK"-button centers the map according to the start and endposition 
getLatLng: gets address-String and build a new markers-array element with latitude and longitude of this address
startOKButton, endOKButton & clockOKButton are only used for coping with asynchronous js bahaviour (f. same)
testID: confirm inputs with enter
(f)startAndEndPointDraw: enable routing and center map to start and end point
removeAllMarkers: removes all markers, routs and local storage
**/

function startAndEndPointDraw() {
	if (isStartPressed===1 && isEndPressed===1  && isClockPressed===1) {
		centerToStartAndEndpoint();
	} else {
		alert('Du musst zuerst beide Adressen und die Uhrzeit mit den Knöpfen rechts neben den Eingabe-Feldern bestätigen');
	}
}

function exportData() {
	var name = 'geojson.txt';
	var i,allDangerFilled=1;
	for(i=0;i<dangerMessage.length;i++) {
		if(dangerMessage[i] == 'undefined') {
			allDangerFilled=0;
			dangerPoints[i].options.icon.options.iconSize=[80,80];
			console.log(dangerPoints[i]);
			map.removeLayer(dangerPoints[i]);
			dangerPoints[i].addTo(map);
		} 
	}
	for(i=0;i<dangerMessage.length;i++) {
		if(JSON.stringify(dangerPoints[i].options.icon.options.iconSize)=="[80,80]" && dangerMessage[i] != 'undefined') {
			dangerPoints[i].options.icon.options.iconSize=[30,30];
		}
	}
	if(isStartPressed==1 && isEndPressed==1 && isClockPressed==1 && isFirstClicked==1 && allDangerFilled==1) {
		var geojson = getGeoJson();
		if(!wantToTestDistance){download(name , geojson);}
	}
	if(allDangerFilled==0) {
		alert('Du hast die hervorgehobenen Gefahrenstellen markiert, aber keinen Grund angegeben!');
	}
}

function centerToStartAndEndpoint() {
	var group = new L.featureGroup(markers);
	map.fitBounds(group.getBounds());
}

function getLatLng(address,i,f) {
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode({'address':address},function(results,status){
				if(status === google.maps.GeocoderStatus.OK) {
					var pos = results[0].geometry.location;	
					var myIconUrl;
					if(i==0) {
						myIconUrl='../../images/circle_blue_start.ico';
						isStartPressed=1;
						if(f=='f') {
							document.getElementById('fstart').style.display='none';
							document.getElementById('fstartOK').style.display='none';
							document.getElementById('fend').style.display='block';
							document.getElementById('fendOK').style.display='block';
							document.getElementById('fend').focus();
					}
					} else{
						myIconUrl='../../images/circle_red_end.ico';
						isEndPressed=1;
						if(f=='f') {
							document.getElementById('fdate').style.display='block';
							document.getElementById('ftime').style.display='block';
							document.getElementById('fclockOK').style.display='block';
							document.getElementById('fend').style.display='none';
							document.getElementById('fendOK').style.display='none';
						}
					}
					var personalizedIcon = L.icon({
						iconUrl: myIconUrl,
						iconSize: [80,80]
					});
						markers[i] = L.marker([pos.lat(),pos.lng()],{icon:personalizedIcon});
					if(i==0) {
						markers[0].bindPopup("Klicke in den blauen Kreis um den ersten Wegpunkt zu zeichnen!",{closeButton:false});
						markers[0]._popup.options.offset.y = -50;
						markers[0].on('click',function(e){
							addMarker(e);
							isFirstClicked=1;
							markers[0].closePopup();
						});
					}
					if(i==1) {
						markers[1].on('click',function(e){
							if(isFirstClicked==1) {
								addMarker(e);
								addRoute(markers.length-4);
								changeIcon(markers.length-2);
							}
						});
						markers[0].openPopup();
					}
					markers[i].addTo(map);
				}
				else {
					alert('Eine Addresse gibt es nicht. Kannst du deine Eingaben noch einmal überprüfen?');
					console.log('Geocode failed because of ' + status);
					if(i==0 && isStartPressed==0 && f=='f') {
						document.getElementById('fstart').style.display='block';
						document.getElementById('fstartOK').style.display='block';
						document.getElementById('fend').style.display='none';
						document.getElementById('fendOK').style.display='none';
					}
					if(i==1 && isEndPressed==0 && f=='f') {
						document.getElementById('fdate').style.display='none';
						document.getElementById('ftime').style.display='none';
						document.getElementById('fclockOK').style.display='none';
						document.getElementById('fend').style.display='block';
						document.getElementById('fendOK').style.display='block';
					}
				}
	});
}

function startOKButton() {
	var i,address = document.getElementById('start').value;
	localStorage.setItem('address1',address);
	if (isStartPressed==1) {
		for(i=0;i<markers.length;i++) {
			if (i==1) {continue;}
			map.removeLayer(markers[i]);
		}
		for(i=0;i<route.length;i++) {
			route[i].removeFrom(map);
			if(typeof lines[i] != 'undefined') {map.removeLayer(lines[i]);}
		}
		route=[];
		lines=[];
		markers=[];
	}
	if (address==='') {
		alert('Du hast das Startfeld leer gelassen');
	} else {
		markers[0] = getLatLng(address,0,'a');
		isStartPressed=1;
	}
}

function endOKButton() {
	var i,address = document.getElementById('end').value;
	localStorage.setItem('address2',address);
	if (isEndPressed===1) {map.removeLayer(markers[1]);}
	if (address==='') {
		alert('Du hast das Zielfeld leer gelassen');
	} else {
		markers[1] = getLatLng(address,1,'a');
		isEndPressed=1;
	}
}

function clockOKButton() {
	var isnum=true;
	startTime = document.getElementById('time').value;
	startDate = document.getElementById('date').value;
	if(isClockPressed===1) {/*ändere Zeitpunkt in popup*/}
	if (startDate=='' || startTime=='') {
		alert('Du hast ein Feld beim Datum oder der Uhrzeit nicht ausgefüllt!');
		isnum=false;
	}
	if(isnum) {
		isClockPressed=1;
	}
}

function removeAllMarkers() {
	var i;
	for(i=0;i<markers.length;i++) {
		map.removeLayer(markers[i]);
	}
	markers=[];
	for(i=0;i<route.length;i++) {
		if(route[i]!='undefined'){route[i].removeFrom(map);}
		if(typeof lines[i] != 'undefined') {map.removeLayer(lines[i]);}
	}
	route=[];
	coord=[];
	lines=[];
	isStartPressed = 0;
	isEndPressed = 0;
	isClockPressed = 0;
	isFirstClicked = 0;
	clearStorage();
	document.getElementById('time').value = '';
	document.getElementById('date').value = '';
	document.getElementById('start').value = '';
	document.getElementById('end').value = '';
	document.getElementById('ftime').value = '';
	document.getElementById('fdate').value = '';
	document.getElementById('fstart').value = '';
	document.getElementById('fend').value = '';
	document.getElementById("famount").selectedIndex = "0";
	initialize();
}

function fstartOKButton() {
	var address = document.getElementById('fstart').value;
	localStorage.setItem('address1',address);
	if (isStartPressed==1) {
		map.removeLayer(markers[0]);
	}
	if (address==='') {
		alert('Du hast das Startfeld leer gelassen');
	} else {
		markers[0] = getLatLng(address,0,'f');
	}
}

function fendOKButton() {
	var address = document.getElementById('fend').value;
	localStorage.setItem('address2',address);
	if (isEndPressed===1) {map.removeLayer(markers[1]);}
	if (address==='') {
		alert('Du hast das Zielfeld leer gelassen');
	} else {
		markers[1] = getLatLng(address,1,'f');
		isEndPressed=1;
	}
}

function fclockOKButton() {
	var isnum=true;
	startTime = document.getElementById('ftime').value;
	startDate = document.getElementById('fdate').value;
	var sdzw= startDate.split('-');
	var d = new Date(sdzw[0],sdzw[1]-1,sdzw[2]);
	var weekday = ["Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag"];
	document.getElementById('famount').options[1].text = 'Nur am ' + sdzw[2] + '.' + sdzw[1] + '.' + sdzw[0];
	document.getElementById('famount').options[2].text = 'Immer am ' + weekday[d.getDay()] + ' außer in den Ferien';
	document.getElementById('famount').options[3].text = 'Immer am ' + weekday[d.getDay()];
	
	if (startDate!='' && startTime!='') {
		document.getElementById('fdate').style.display='none';
		document.getElementById('ftime').style.display='none';
		document.getElementById('fclockOK').style.display='none';
		document.getElementById('famount').style.display='block';
		document.getElementById('famountOK').style.display='block';
	} else {
		alert('Du hast ein Feld beim Datum oder der Uhrzeit nicht ausgefüllt!');
		isnum=false;
	}
	if(isnum) {
		isClockPressed=1;
	}
	if(isClockPressed===1) {/*ändere Zeitpunkt in popup*/}
}

function fstartAndEndPointDraw() {
	
	document.getElementById('fstartRouting').style.display='none';
	showAllInputs();
	if (isStartPressed===1 && isEndPressed===1  && isClockPressed===1) {
		centerToStartAndEndpoint();
	} else {
		alert('Du musst zuerst beide Adressen und die Uhrzeit mit den Knöpfen rechts neben den Eingabe-Feldern bestätigen');
	}
	document.getElementById('firstInput').style.zIndex="-1";
	document.getElementById('transportation').style.zIndex="3";
}

function famountOK() {
	if(amount!=0) {
		document.getElementById('famount').style.display='none';
		document.getElementById('famountOK').style.display='none';
		document.getElementById('fstartRouting').style.display='block';
	}
}

function get_amount(e) {
	amount = e.options[e.selectedIndex].value;
}

function testID(arg) {
	var id = arg.getAttribute('id');
	if(event.keyCode == 13) {
		switch (id) {
			case 'start':
				startOKButton();
				break;
			case 'end':
				endOKButton();
				break;
			case 'fstart':
				fstartOKButton();
				break;
			case 'fend':
				fendOKButton();
				break;
			case 'famount':
				famountOK();
				break;
			default:
				console.log('something went wrong');	
		}
	}	
}

function changeTransportation(arg) {
	id = arg.getAttribute('id');
	var value = document.getElementById(id).value;
	var pos = value * 20 - 4 + '%';
	document.getElementById('rectangle').style.top = pos;
	vehicle = id;
	if(vehicle=='bike' && routers == 'mapzen') {vehicle='bicycle';}
}

function changePicToExpl(arg) {
	var value = arg.getAttribute('value');
	var id = arg.getAttribute('id');
	document.getElementById(id).src = '../images/'+id+value+'.png';
}

function changeExplToPic(arg) {
	var value = arg.getAttribute('value');
	var id = arg.getAttribute('id');
	document.getElementById(id).src = '../images/'+id+'.png';
}


// LOCAL STORAGE
/**
Deal with storing,reading and deleting the local storage 
[problem: objects have to be converted to a string and after loading separated again]
**/

function storeValue() {
	if (markers.length>0) {
		var i=0,markersString='',choiceString='';
		for(i=0;i<markers.length-1;i++) {
			markersString += markers[i]._latlng + '|';
		}
		for(i=0;i<choiceOfVeh.length;i++) {
			choiceString += choiceOfVeh[i] + '|';
		}
		markersString += markers[markers.length-1]._latlng;
		if (localStorage) {
			localStorage.setItem('markers', markersString);
			localStorage.setItem('device', choiceOfVeh);
		}
	}
	if (startTime!=null || startTime!='') {
		localStorage.setItem('time',startTime);
	}
	if (startDate!=null || startDate!='') {
		localStorage.setItem('date',startDate);
	}
	if (amount!=0) {
		localStorage.setItem('amount',amount);
	}
}

function getStoredValue(key) {
	if(localStorage.getItem(key)!=null) {
		var str;
		if (localStorage) {
			str = localStorage.getItem(key);
		}
		return str.split("|");
	}
}

function getStoredMarkers(storedMarkers) {
	var i;
	for (i=0;i<storedMarkers.length;i++) {
		var zw1,zw2,zw3,zw4={};
		zw1=storedMarkers[i].split(")");
		zw2=zw1[0].split("(");
		zw3=zw2[1].split(",");
		zw4.lat=zw3[0];
		zw4.lng=zw3[1];
		var myIcon;
		if(i!=0 && i!=1) {
			var e = new Object();
			e.latlng = new Object();
			e.latlng.lat = zw4.lat;
			e.latlng.lng = zw4.lng;
			if(i>2){
				vehicle = storedDevice[i-3];
			}
			addMarker(e);
			if(markers.length>3) {
				addRoute(markers.length-4);
			}
			changeIcon(markers.length-2);
		}
		if(i==0) {
			myIcon = L.icon({iconUrl:'../images/circle_blue_start.ico' , iconSize :[80,80]}); 
			markers[0] = L.marker([zw4.lat,zw4.lng],{icon:myIcon});
			markers[0].bindPopup("Klicke in den blauen Kreis um den ersten Wegpunkt zu zeichnen!",{closeButton:false});
			markers[0]._popup.options.offset.y = -50;
			markers[0].on('click',function(e){
				addMarker(e);
				isFirstClicked=1;
				markers[1].on('click',function(e){
					if(isFirstClicked==1) {
						addMarker(e);
						addRoute(markers.length-4);
						changeIcon(markers.length-2);
					}
				});
				markers[0].closePopup();
			});
			markers[0].addTo(map);
		}
		if(i==1) {
			myIcon = L.icon({iconUrl:'../images/circle_red_end.ico' , iconSize :[80,80]}); 
			markers[1] = L.marker([zw4.lat,zw4.lng],{icon:myIcon});
			markers[1].addTo(map);
			markers[1].on('click',function(e){
				if(isFirstClicked==1) {
					addMarker(e);
					addRoute(markers.length-4);
					changeIcon(markers.length-2);
				}
			});
		}
	}
	if(isFirstClicked==0) {
		markers[0].openPopup();	
	} else {
		markers[0].closePopup();
	}
}

function clearStorage() {
	localStorage.removeItem('markers');
	localStorage.removeItem('time');
	localStorage.removeItem('date');	
	localStorage.removeItem('address1');
	localStorage.removeItem('address2');
	localStorage.removeItem('device');
	localStorage.removeItem('amount');
	localStorage.removeItem('isEndClicked');
}


// EXPORT DATA
/**
getGeoJson: get a geojson compatible string out of markers array (only coordinates), time Zone, times of destination at spliced route for exporting
getDistance: calculates distance between two latLng elements
download: download a .txt-file
prepareCoords: coordinates which appear twice in a row were deleted
getDistCoord: get distance array
split: get an array of coordinates which distance is less than 1
getTimeOfWaypoints: get time of destination at each splitted waypoint
**/

function getGeoJson() {
	var i;
	
	// prepare time and date
	var timeArray = startTime.split(':');
	var dateArray = startDate.split('-');
	var date = new Date(dateArray[0],dateArray[1]-1,dateArray[2],timeArray[0],timeArray[1]);
	var dateSplit = date.toString().split(' ');
	var weekday = ["sunday","monday","tuesday","wednesday","thursday","friday","saturday"];
	var weekly=false,daily=false,onlyAtSchoolday=false;
	switch (amount) {
		case '1':
			break;
		case '2':
			weekly=true;
			onlyAtSchoolday=true;
			break;
		case '3':
			weekly=true;
			break;
		case '4':
			weekly=true;
			daily=true;
			onlyAtSchoolday=true;
			break;
		default: console.log('Wrong frequency of route usage inserted');
	}
	
	
	// define geoJson object and fill properties
	var geoJsonData = new Object();
	geoJsonData.type = "FeatureCollection";
	geoJsonData.features = new Array();
	geoJsonData.properties = new Object();
	geoJsonData.properties.timeStamp = date;
	geoJsonData.properties.timeZone = dateSplit[5] + ' ' + dateSplit[6] + ' ' + dateSplit[7];
	geoJsonData.properties.frequency = new Object();
	geoJsonData.properties.frequency.day = weekday[date.getDay()];
	geoJsonData.properties.frequency.weekly = weekly;
	geoJsonData.properties.frequency.daily = daily;
	geoJsonData.properties.frequency.onlyAtSchoolday = onlyAtSchoolday;
	
	/* ================================================================================================================================ */
	if(wantToTestDistance) {
		var test=[];
	}
	/* ================================================================================================================================ */
	
	// MultiLineString for route
	prepareCoords();
	for(i=0; i<coord.length;i++) {
		var geoJsonMultiLineFeature = new Object();
		geoJsonMultiLineFeature.type = "Feature";
		geoJsonMultiLineFeature.geometry = new Object();
		geoJsonMultiLineFeature.geometry.type = "MultiLineString";	
		
		var j,routeArray =[],zwArray=[],distance;
		var divCoord=[];
		if(choiceOfVeh[i] == 'foot' || choiceOfVeh[i] == 'bike' || choiceOfVeh[i] == 'bicycle') {
			divCoord = split(coord[i].coordinates);
			distance="1";
			
			/* ================================================================================================================================ */
			if(wantToTestDistance) {
				var k;
				for(k=0;k<divCoord.length-1;k++){
					test.push(getDistance(divCoord[k][1],divCoord[k][0],divCoord[k+1][1],divCoord[k+1][0]));
				}
			}
			/* ================================================================================================================================ */
			
		} else {
			var zw = [coord[i].coordinates[0][1] , coord[i].coordinates[0][0]]; 
			divCoord.push(zw); 
			zw = [coord[i].coordinates[1][1] , coord[i].coordinates[1][0]]; 
			divCoord.push(zw);
			distance = getDistance(divCoord[0][1],divCoord[0][0],divCoord[1][1],divCoord[1][0]);
		}
		for(j=0;j<divCoord.length;j++) {
			zwArray[j]=[divCoord[j][0] , divCoord[j][1]];
		}
		routeArray.push(zwArray);
		geoJsonMultiLineFeature.geometry.coordinates = routeArray;
		
		geoJsonMultiLineFeature.properties = new Object();
		var devZw = choiceOfVeh[i];
		if(devZw=='bicycle'){devZw='bike';}
		geoJsonMultiLineFeature.properties.vehicle = devZw;
		geoJsonMultiLineFeature.properties.distance = distance;
		geoJsonMultiLineFeature.properties.name = "Route";
		geoJsonData.features.push(geoJsonMultiLineFeature);
	}
	    
	/* ================================================================================================================================ */
	if(wantToTestDistance) {
		var max,min,k,av=0,plus=0,minus=0;
		max = Math.max.apply(Math, test); 
		min = Math.min.apply(Math, test);
		for(k=0;k<test.length;k++){
			av+=test[k];
			if(test[k]<=DIST*0.9) {minus++;}
			if(test[k]>DIST) {plus++;}
		}
		console.log('distances: ' , test);
		console.log('index of max: ' + test.indexOf(max) + '     |     max: ' + max);
		console.log('index of min: ' + test.indexOf(min) + '     |     min: ' + min);
		console.log('amount of < '+DIST*0.9+'m: ' + minus + '  |  amount of >'+DIST+'m: ' + plus);
		console.log('average distance: ' + av/test.length);
		console.log('percentage of errors: '+Math.round(100*100*(minus+plus)/test.length)/100+'%');
	}
	/* ================================================================================================================================ */
	
	// separat points for danger message
	for(i=0;i<dangerPoints.length;i++) {
		var geoJsonDangerReport = new Object();
		geoJsonDangerReport.type = "Feature";
		geoJsonDangerReport.geometry = new Object();
		geoJsonDangerReport.geometry.type = "Point"
		geoJsonDangerReport.geometry.coordinates = [dangerPoints[i]._latlng.lng , dangerPoints[i]._latlng.lat];
		geoJsonDangerReport.properties = new Object();
		geoJsonDangerReport.properties.reason = dangerMessage[i];
		geoJsonDangerReport.properties.name = "Danger Report"
		geoJsonData.features.push(geoJsonDangerReport);
	}

	// stringify geoJson object
	var geoJsonString = JSON.stringify(geoJsonData);
	return geoJsonString;
}

function getDistance(lat1, lng1, lat2, lng2){ 
    var R = 6371.000785; // Radius of earth (averaged) in KM (äquatorial: 6378.137 km)
    var dLat = (lat2 - lat1) * Math.PI / 180;
    var dLng = (lng2 - lng1) * Math.PI / 180;
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
			Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
			Math.sin(dLng/2) * Math.sin(dLng/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    return d * 1000; // meters
}

function download(filename, content) {
    var pom = document.createElement('a');
    pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(content));
    pom.setAttribute('download', filename);
    pom.click();
}

function prepareCoords() {
	var i;
	for(i=0; i<coord.length || i<lines.length; i++) {
		if(typeof coord[i] == 'undefined') {
			coord[i] = new Object();
			coord[i].coordinates = new Array();
			coord[i].coordinates[0] = [lines[i]._latlngs[0].lat , lines[i]._latlngs[0].lng];
			coord[i].coordinates[1] = [lines[i]._latlngs[1].lat , lines[i]._latlngs[1].lng];
		} else {
			if(i!=coord.length) {
				var len = coord[i].coordinates.length-1;
				if(JSON.stringify(coord[i].coordinates[len]) != JSON.stringify([markers[i+3]._latlng.lat , markers[i+3]._latlng.lng])) {
					coord[i].coordinates.push([markers[i+3]._latlng.lat , markers[i+3]._latlng.lng]);
				}
			}
			if(i!=0) {
				if(JSON.stringify(coord[i].coordinates[0]) != JSON.stringify([markers[i+2]._latlng.lat , markers[i+2]._latlng.lng])) {
					var zw = [markers[i+2]._latlng.lat , markers[i+2]._latlng.lng];
					coord[i].coordinates = [zw].concat(coord[i].coordinates);
				}
			}
		}
	}
}

function getDistCoord(coord) {
	var i,dist=[];
	for(i=0;i<coord.length-1;i++) {
		dist[i] = getDistance(coord[i][0],coord[i][1],coord[i+1][0],coord[i+1][1]);
	}
	return dist;
}

function split(a) {
	var j,arr=[];
	var distance = getDistCoord(a);
	for(j=0;j<a.length-1;j++) {
		if(distance[j]>DIST) {
			var i,lam=1/(Math.ceil(distance[j])/DIST);
			for(i=0;i<Math.ceil(distance[j]/DIST);i++) {
				arr.push( [ (1-i*lam)*a[j][1] + i*lam*a[j+1][1], (1-i*lam)*a[j][0] + i*lam*a[j+1][0] ]   );
			}
		} 
		if(j<a.length-1 && distance[j]<=DIST && distance[j-1]<=DIST && j>0) {
			arr.push([a[j-1][1] , a[j-1][0]]);
		}
	}
	arr.push([a[a.length-1][1],a[a.length-1][0]]);
	return arr;
}


// DANGER REPORT
/**

**/

function routingEnded() {
	hideInputs();
	document.getElementById('dangerReport').style.zIndex='3';
	document.getElementById('backToRouting').style.zIndex='3';
	document.getElementById('endOfRoutingAndDanger').style.display='none';
	isEndClicked=1;
	localStorage.setItem('isEndClicked', 1);
	storeValue();
}

function startDangerReport() {
	document.getElementById('endOfRoutingAndDanger').style.display = 'block';
	document.getElementById('backToRouting').style.zIndex='3';
	document.getElementById('dangerReport').style.zIndex='-1';
	prepareMarkersForDangerReport();
	prepareLinesForDangerReport();
}

function prepareMarkersForDangerReport() {
	var i;
	for(i=0;i<markers.length;i++) {
		map.removeLayer(markers[i]);
		var iconZw;
		if(typeof markers[i].options.icon.options.iconUrl != 'undefined') {
			iconZw = L.icon({iconUrl:markers[i].options.icon.options.iconUrl , iconSize : markers[i].options.icon.options.iconSize});
			dangerMarkers[i] = L.marker(markers[i]._latlng , {'draggable':false, 'icon': iconZw });
		} else {
			dangerMarkers[i] = L.marker(markers[i]._latlng , {'draggable': false });
		}
		if(i>1) {
			dangerMarkers[i].on('click',function(e){
				var i,test=true;
				for(i=0;i<dangerPoints.length;i++) {
					if(JSON.stringify(dangerPoints[i]._latlng)==JSON.stringify(e.latlng)) {
						test=false;
					}
				}
				if(test) {
					addDangerMarker(e);
				}	
			});
		}
		dangerMarkers[i].addTo(map);
	}
}

function prepareLinesForDangerReport() {
	var i;
	for(i=0;i<markers.length-3;i++) {
		if(typeof lines[i] != 'undefined') {
			lines[i].options.clickable = true;
			lines[i].on('click',function(e){addDangerMarker(e);});
			// Eigentlich unnötig, da dort keine Gefahrensituationen auftreten!
		} 
	}
}

function getDangerMessage(arg) {
	var id = arg.getAttribute('id');
	dangerMessage[id.split('s')[1]] = document.getElementById(id.split('OK')[1]).value;
	if(dangerMessage[id.split('s')[1]]!='') {
		map.removeLayer(dangerPoints[id.split('s')[1]]);
		dangerPoints[id.split('s')[1]].closePopup();
		dangerPoints[id.split('s')[1]].bindPopup(
			'<input type="text" name=' +id.split('OK')[1]+ ' id=' +id.split('OK')[1]
			+ ' placeholder="Warum ist diese Stelle gefährlich?" value="' +dangerMessage[id.split('s')[1]]+ '" style="width:310px;height:40px;font-size:20px;"/>'
			+'<input type="button" id='+id+' value="OK" onclick="getDangerMessage(this)" style="width:155px;"/>'
			+'<input type="button" value="Löschen" class="marker-delete-button" style="width:155px;"/>'
		);
		dangerPoints[id.split('s')[1]]._popup.options.minWidth=330;
		dangerPoints[id.split('s')[1]].options.icon.options.iconSize=[30,30];
		dangerPoints[id.split('s')[1]].addTo(map);
	} 
}

function addDangerMarker(e) {
	var iconZw = L.icon({iconUrl:'../images/danger.ico' , iconSize:[30,30]});
	var newMarker = L.marker(e.latlng,{icon:iconZw});
	var zw = "dangerPoints" + dangerPoints.length;
	var zw2 = '"OK' + zw + '"'; 
	zw = '"' + zw + '"';
	newMarker.bindPopup(
		'<input type="text" name='+zw+' id='+zw+' placeholder="Warum ist diese Stelle gefährlich?" style="width:310px;height:40px;font-size:20px;"/>'
		+'<input type="button" id='+zw2+' value="OK" onclick="getDangerMessage(this)" style="width:155px;"/>'
		+'<input type="button" value="Löschen" class="marker-delete-button" style="width:155px;"/>'
	);
	newMarker.on('popupopen', onPopupOpen);
	newMarker._popup.options.minWidth=330;
	dangerPoints.push(newMarker);
	dangerPoints[dangerPoints.length-1].addTo(map);
	dangerMessage.push('undefined');
}

function backToRouting() {
	document.getElementById('backToRouting').style.zIndex='-1';
	localStorage.removeItem('isEndClicked');
	isEndClicked=0;
	var i;
	for(i=0;i<dangerPoints.length;i++) {
		console.log('test');
		map.removeLayer(dangerPoints[i]);
		if(typeof unfilledDangerMessage[i] != 'undefined'){map.removeLayer(unfilledDangerMessage[i]);}
	}
	if(typeof dangerMarkers[0] != 'undefined') {
		for(i=0;i<markers.length;i++){
			map.removeLayer(dangerMarkers[i]);
		}
	} else {
		for(i=0;i<markers.length;i++) {
			map.removeLayer(markers[i]);
		}
	}
	for(i=0;i<route.length;i++) {
		if(route[i]!='undefined'){route[i].removeFrom(map);}
		if(typeof lines[i]!='undefined'){map.removeLayer(lines[i]);}
	}
	initialize();
}

function downloadMap() {
	leafletImage(map, function(err, canvas) {
    // now you have canvas
    // example thing to do with that canvas:
    var img = document.createElement('img');
    var dimensions = map.getSize();
    img.width = dimensions.x;
    img.height = dimensions.y;
    img.src = canvas.toDataURL();
    document.getElementById('images').innerHTML = '';
    document.getElementById('images').appendChild(img);
	});
}


// OTHERS
/**

**/

function openPopupByRightMouse(e) {
	if(e.which == 3) {
		var x = e.x;
		var y = e.y;
		var len = markers.length;
		var len2 = dangerPoints.length;
		var zwVeh2 = vehicle;
		vehicle = 'car';
		simulateClick(x,y);
		if(len != markers.length || len2!=dangerPoints.length){ 		
			var index = markers.length-1;
			if(isEndClicked==0) {
				if(index>3) {
					var zwVeh = choiceOfVeh[index-4];
					if(zwVeh=='bicycle'){zwVeh='bike';}
					if(zwVeh=='bus'){zwVeh='H';}
					map.removeLayer(markers[index-1]);
					markers[index-1].options.icon = L.icon({iconUrl:'../images/'+zwVeh+'.png' , iconSize :[40,40]});
					markers[index-1].addTo(map);
				}
				deleteMarker(index);
				deleteRoute(index);
			} else {
				var index2 = dangerPoints.length-1;
				map.removeLayer(dangerPoints[index2]);
				dangerPoints.splice(index2,1);
				dangerMessage.splice(index2,1);
			}
		}
		vehicle = zwVeh2;
	}
}

function simulateClick(x, y) {
    var clickEvent= document.createEvent('MouseEvents');
    clickEvent.initMouseEvent(
		'click', true, true, window, 0,
		0, 0, x, y, false, false,
		false, false, 0, null
    );
    document.elementFromPoint(x, y).dispatchEvent(clickEvent);
}









